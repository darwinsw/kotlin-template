package com.darwinsw.templates.kotlin

class Hello {
    fun sayHello(to: String) = "Hello, ${to}!"
}